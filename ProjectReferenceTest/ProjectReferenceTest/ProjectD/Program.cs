﻿using System;
using ProjectC;

namespace ProjectD
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var obj = new Class3();
                Console.WriteLine("All good");
            }
            catch (Exception e)
            {
                Console.WriteLine($@"Failed with exception detail '{e}'");
                throw;
            }
        }
    }
}
